const key = "chat";

export function setData(data) {
  // let dataHash = CryptoJS.AES.encrypt(data, key);

  localStorage.setItem(key, JSON.stringify(data));
}

export function getData() {
  return JSON.parse(localStorage.getItem(key));
}

export function logout() {
  localStorage.clear();
}
