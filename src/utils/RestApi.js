import request from "superagent";

export function call(path, dataSend) {
  return request
    .post("http://192.168.0.53:3000/api/v1/" + path)
    .send(dataSend)
    .set("Accept", "application/json");
}
