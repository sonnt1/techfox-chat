import React, { Component } from "react";
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon,
  Image
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import request from "superagent";
import { connect } from "react-redux";
import { setUser, clearUser } from "../../action";
import { setData } from "../../utils/Storage";
import { Logo } from "../../resouces/image";
import { call } from "../../utils/RestApi";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: [],
      loading: false,
      history: this.props.history
    };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  displayError = errors =>
    errors.map((error, index) => <h4 key={index}>{error.message}</h4>);

  isFormValid = ({ email, password }) => email && password;

  handleSubmit = e => {
    const { email, password } = this.state;
    e.preventDefault();
    if (this.isFormValid(this.state)) {
      this.setState({ errors: [], loading: true });
      const sendData = { email, password };
      call("auth/sign_in", sendData)
        .then(res => {
          this.props.setUser(res.body.data);
          this.setState(
            {
              loading: false
            },
            () => {
              this.props.history.push("/");
              setData(res.body.data);
            }
          );
        })
        .catch(err => {
          const error = { message: err.message };
          this.setState({
            errors: this.state.errors.concat(error),
            loading: false
          });
        });
    }
  };

  handleInputError = (errors, inputName) => {
    return errors.some(err => err.message.toLowerCase().includes(inputName))
      ? "error"
      : "";
  };

  render() {
    const { password, email, errors, loading } = this.state;

    return (
      <Grid textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" icon color="orange" textAlign="center">
            <Icon.Group>
              <Image src={Logo} />
            </Icon.Group>
            Login for DevChat
          </Header>
          <Form size="large" onSubmit={this.handleSubmit}>
            <Segment stacked>
              <Form.Input
                fluid
                name="email"
                icon="mail"
                iconPosition="left"
                placeholder="Email"
                onChange={this.handleChange}
                value={email}
                type="email"
                className={this.handleInputError(errors, "email")}
              />
              <Form.Input
                fluid
                name="password"
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                onChange={this.handleChange}
                value={password}
                type="password"
                className={this.handleInputError(errors, "password")}
              />

              <Button
                disabled={loading}
                className={loading ? "loading" : ""}
                color="orange"
                fluid
                size="large"
              >
                Submit
              </Button>
            </Segment>
          </Form>
          {errors.length > 0 && (
            <Message error>
              <h3>Error</h3>
              {this.displayError(errors)}
            </Message>
          )}
          <Message>
            Don't have account? <Link to="/register">Register</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

export default connect(null, { setUser, clearUser })(Login);
