import React, { Component } from "react";
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon,
  Image
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import request from "superagent";
import { connect } from "react-redux";
import { setUser, clearUser } from "../../action";
import { setData } from "../../utils/Storage";
import { Logo } from "../../resouces/image";
import { call } from "../../utils/RestApi";
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      passwordConfirm: "",
      errors: [],
      loading: false
    };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  isFormEmpty = ({ username, email, password, passwordConfirm }) => {
    return (
      !email.length ||
      !password.length ||
      !passwordConfirm.length ||
      !username.length
    );
  };

  isPasswordValid = ({ password, passwordConfirm }) => {
    if (password.length < 6 || passwordConfirm.length < 6) {
      return false;
    } else if (password !== passwordConfirm) {
      return false;
    } else {
      return true;
    }
  };

  isFormValid = () => {
    let errors = [];
    let error;

    if (this.isFormEmpty(this.state)) {
      error = { message: "Fill in all fields" };
      this.setState({ errors: errors.concat(error) });
      return false;
    } else if (!this.isPasswordValid(this.state)) {
      error = { message: "Password is invalid" };
      this.setState({ errors: errors.concat(error) });
      return false;
    } else {
      //form valid
      return true;
    }
  };

  displayError = errors =>
    errors.map((error, index) => <h4 key={index}>{error.message}</h4>);

  handleSubmit = e => {
    const { email, username, password, passwordConfirm } = this.state;
    e.preventDefault();
    if (this.isFormValid()) {
      this.setState({ errors: [], loading: true });
      const dataSend = {
        email,
        password,
        password_confirmation: passwordConfirm,
        name: username
      };
      call("auth", dataSend)
        .then(res => {
          this.props.setUser(res.body.data);
          this.setState(
            {
              loading: false
            },
            () => {
              this.props.history.push("/");
              setData(res.body.data);
            }
          );
        })
        .catch(err => {
          const error = { message: err.response.body.errors.full_messages[0] };
          this.setState({
            errors: this.state.errors.concat(error),
            loading: false
          });
        });
    }
  };

  handleInputError = (errors, inputName) => {
    return errors.some(err => err.message.toLowerCase().includes(inputName))
      ? "error"
      : "";
  };

  render() {
    const {
      username,
      password,
      email,
      passwordConfirm,
      errors,
      loading
    } = this.state;

    return (
      <Grid textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" icon color="violet" textAlign="center">
            <Icon.Group>
              <Image src={Logo} />
            </Icon.Group>
            Register for DevChat
          </Header>
          <Form size="large" onSubmit={this.handleSubmit}>
            <Segment stacked>
              <Form.Input
                fluid
                name="username"
                icon="user"
                iconPosition="left"
                placeholder="Username"
                onChange={this.handleChange}
                value={username}
                type="text"
              />
              <Form.Input
                fluid
                name="email"
                icon="mail"
                iconPosition="left"
                placeholder="Email"
                onChange={this.handleChange}
                value={email}
                type="email"
                className={this.handleInputError(errors, "email")}
              />
              <Form.Input
                fluid
                name="password"
                icon="lock"
                iconPosition="left"
                placeholder="Password"
                onChange={this.handleChange}
                value={password}
                type="password"
                className={this.handleInputError(errors, "password")}
              />
              <Form.Input
                fluid
                name="passwordConfirm"
                icon="repeat"
                iconPosition="left"
                placeholder="Password Confirm"
                onChange={this.handleChange}
                value={passwordConfirm}
                type="password"
                className={this.handleInputError(errors, "password")}
              />

              <Button
                disabled={loading}
                className={loading ? "loading" : ""}
                color="violet"
                fluid
                size="large"
              >
                Submit
              </Button>
            </Segment>
          </Form>
          {errors.length > 0 && (
            <Message error>
              <h3>Error</h3>
              {this.displayError(errors)}
            </Message>
          )}
          <Message>
            Already have account? <Link to="/login">Login</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

export default connect(null, { setUser, clearUser })(Register);
