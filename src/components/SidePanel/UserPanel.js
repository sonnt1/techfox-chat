import React, { Component } from "react";
import { Grid, Header, Icon, Dropdown, Image } from "semantic-ui-react";
import { logout } from "../../utils/Storage";
class UserPanel extends Component {
  state = {
    user: this.props.currentUser,
    history: this.props.history
  };

  dropdownOptions = () => [
    {
      key: "user",
      text: (
        <span>
          Sign In as{" "}
          <strong>{this.state.user ? this.state.user.name : ""}</strong>
        </span>
      ),
      disabled: true
    },
    {
      key: "avatar",
      text: <span>Change Avatar</span>,
      disabled: false
    },
    {
      key: "signout",
      text: <span onClick={this.handleSignOut}>Sign Out</span>
    }
  ];

  handleSignOut = () => {
    logout();
    this.state.history.push("/login");
  };

  render() {
    const { user } = this.state;
    console.log(this.state);
    console.log(user && user.email);
    return (
      <Grid style={{ background: "#2851A3" }}>
        <Grid.Column>
          <Grid.Row style={{ padding: "1.2em", margin: 0 }}>
            {/* Header */}
            <Header inverted floated="left" as="h2">
              <Icon name="code" />
              <Header.Content>TechFoxChat</Header.Content>
            </Header>
            {/* End Header */}
            {/* User */}
            <Header style={{ padding: "0.25em" }} as="h4" inverted>
              <Dropdown
                trigger={
                  <span>
                    <Image
                      src={
                        user && user.avatar
                          ? user.avatar
                          : `https://api.adorable.io/avatars/100/${user &&
                              user.email}.png
                      `
                      }
                      spaced="right"
                      avatar
                    />
                    {user ? user.name : ""}
                  </span>
                }
                options={this.dropdownOptions()}
              ></Dropdown>
            </Header>
            {/* End User */}
          </Grid.Row>
        </Grid.Column>
      </Grid>
    );
  }
}

export default UserPanel;
