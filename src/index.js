import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";
import firebase from "firebase";
import App from "./components/App";
import registerServiceWorker from "./registerServiceWorker";
import "semantic-ui-css/semantic.min.css";

import { createStore } from "redux";
import { Provider, connect } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { Loading } from "./loading";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter
} from "react-router-dom";
import rootReducer from "./reducers";
import { getData } from "./utils/Storage";
import { setUser, clearUser } from "./action";

const store = createStore(rootReducer, composeWithDevTools());

class Root extends React.Component {
  componentDidMount() {
    let data = getData();
    console.log("data", data, this.props.user);
    if (data) {
      this.props.setUser(data);
      this.props.history.push("/");
    } else {
      this.props.clearUser();
      this.props.history.push("/login");
    }
  }

  render() {
    return this.props.user.loading ? (
      <Loading />
    ) : (
      <Switch>
        <Route exact path="/" component={App} {...this.props} />
        <Route path="/login" component={Login} {...this.props} />
        <Route path="/register" component={Register} {...this.props} />
      </Switch>
    );
  }
}

const mapStateFromProps = ({ user }) => ({
  user
});

const RootWithAuth = withRouter(
  connect(mapStateFromProps, { setUser, clearUser })(Root)
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <RootWithAuth />
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
